package kriskovic.zavrsni.service;

// import java.io.IOException;

import kriskovic.zavrsni.model.Korisnik;

public interface IKorisnikService {
    void saveKorisnik(Korisnik korisnik);
    
    // void saveKorisnik(Korisnik korisnik) throws IOException;

    void updateKorisnik(Korisnik korisnik);

    Korisnik logginKorisnik(String email, String password);

    Korisnik getKorisnikById(long id);
    
    Korisnik getKorisnikByEmail(String email);

    void registerOrDeleteKorisnik(long id, boolean register);

    boolean checkIfExists(Korisnik korisnik);
    
    void deleteByEmail(String email);

    void deleteKorisnik(Korisnik korisnik);

    void deleteKorisnikById(Long id);
}
