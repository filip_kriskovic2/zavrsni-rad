package kriskovic.zavrsni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

import kriskovic.zavrsni.model.Cilj;
import kriskovic.zavrsni.repository.CiljRepository;

@Service
public class CiljService
{
    @Autowired
    private CiljRepository ciljRepo;

    @Transactional
    public void deleteCiljById(int id) {
        ciljRepo.deleteByIdCilja(id);
    }

    public void saveCilj(Cilj cilj) {
        this.ciljRepo.save(cilj);
        System.out.println("\u001B[32m"+"\nSPREMAMO CILJ\n"+"\u001B[0m");
        System.out.println(cilj);
    }

    public List<Cilj> getAllWithName(String nacin) {
        return ciljRepo.findAllByNacin(nacin);
    }

    public List<Cilj> getAll() {
        return ciljRepo.findAll();
    }

    public List<Cilj> getAllByEmail(String email) {
        return ciljRepo.findAllByEmail(email);
    }
    
}
