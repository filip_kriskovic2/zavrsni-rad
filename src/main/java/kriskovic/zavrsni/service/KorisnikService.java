package kriskovic.zavrsni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import kriskovic.zavrsni.model.Korisnik;
import kriskovic.zavrsni.repository.KorisnikRepository;

@Service
public class KorisnikService implements IKorisnikService
{
    @Autowired
    private KorisnikRepository korisnikRepo;

    @Override
    public void saveKorisnik(Korisnik korisnik) {
        System.out.println(korisnik);
        if (korisnik.signinWrong())
        {
            System.out.println("\u001B[31m"+"\nNEPOTPUN REGISTER\n"+"\u001B[0m");
            throw new RuntimeException("Some fields are missing\n");
        }
        else if (this.checkIfExists(korisnik))
        {
            System.out.println("\u001B[31m"+"\nNEPOTPUN REGISTER\n"+"\u001B[0m");
            throw new RuntimeException("This email address is already registered");
        }
        String msg = korisnik.validateUser();
        if (msg != "OK") {
            throw new RuntimeException(msg);
        }
        this.korisnikRepo.save(korisnik);
        System.out.println("\u001B[32m"+"\nSPREMAMO KORISNIKA\n"+"\u001B[0m");
        System.out.println(korisnik);
        
    }

    @Override
    public void updateKorisnik(Korisnik korisnik) {
        Korisnik oldKorisnik = this.getKorisnikByEmail(korisnik.getEmail());
        if (oldKorisnik == null) {
            String error = String.format("User with this email does not exist: %s\n", korisnik.getEmail());
            throw new RuntimeException(error);
        }
        String msg = korisnik.validateUser();
        if (msg != "OK") {
            throw new RuntimeException(msg);
        }
        korisnikRepo.save(korisnik);        
    }

    @Override
    public Korisnik logginKorisnik(String email, String password) {
        Korisnik mybKor = this.getKorisnikByEmail(email);
        byte[] lozinka = mybKor.getLozinka();
        System.out.println(Arrays.toString(lozinka));
        byte[] salt = new byte[16];
        System.arraycopy(lozinka, 0, salt, 0, 16);
        byte[] pass = new byte[16];
        System.arraycopy(lozinka, 16, pass, 0, 16);
        System.out.println(Arrays.toString(salt));
        System.out.println(Arrays.toString(pass));
        // System.out.println(salt.getBytes(Charset.forName("UTF-8")));
        PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 1000000, 128);
        SecretKeyFactory skf = null;
        byte[] secretkey;
        try {
            skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            secretkey = skf.generateSecret(spec).getEncoded();
            // System.out.println(skf.generateSecret(spec).getEncoded());
            System.out.println(Arrays.toString(secretkey));
            // System.out.println(pass);
            // System.out.println(lozinka + "\n" + salt + "\n" + pass + "\n" + secretkey);
        } catch (NoSuchAlgorithmException ignore) {
            return null;
        }
        catch (InvalidKeySpecException e) {
            throw new IllegalArgumentException(e);
        }
        if (mybKor == null || !(Arrays.equals(pass, secretkey)))
        // if (mybKor == null || !(lozinka.equals(password)))
        {
            System.out.printf("Neuspjeli login\n");
            throw new RuntimeException("Invalid login");
        }
        System.out.printf("Uspjeli login %s\n", mybKor);
        return mybKor;
    }

    @Override
    public Korisnik getKorisnikById(long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Korisnik getKorisnikByEmail(String email) {
        Optional<Korisnik> optional = korisnikRepo.getByEmail(email);
        if (optional.isPresent())
        {
            return optional.get();
        }
        else
        {
            throw new RuntimeException("There is no user with this email: " + email);
        }
    }

    @Override
    public void registerOrDeleteKorisnik(long id, boolean register) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean checkIfExists(Korisnik korisnik)
    {
        List<Korisnik> sviKorisnici = this.getAllUsers();
        for (Korisnik k : sviKorisnici)
        {
            if ( korisnik.getEmail().equals(k.getEmail()) )
            {
                System.out.println("\u001B[31m"+"\n!!!KORISNIK POSTOJI\n"+"\u001B[0m");
                return true;
            }
        }
        return false;
    }

    private List<Korisnik> getAllUsers() {
        return this.korisnikRepo.getAllUsers();
    }

    @Override
    public void deleteByEmail(String email) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void deleteKorisnik(Korisnik korisnik) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void deleteKorisnikById(Long id) {
        // TODO Auto-generated method stub
        
    }

    public Korisnik getKorisnikByEmailJoinCilj(String email) {
        return korisnikRepo.getKorisnikByEmailJoinCilj(email);
    }
    
}
