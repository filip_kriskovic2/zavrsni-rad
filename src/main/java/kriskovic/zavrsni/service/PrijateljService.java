package kriskovic.zavrsni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import kriskovic.zavrsni.model.Prijatelj;
import kriskovic.zavrsni.repository.PrijateljRepository;

@Service
public class PrijateljService {

    @Autowired
    private PrijateljRepository prijateljRepo;

    public void savePrijatelj(Prijatelj prijatelj) {
        this.prijateljRepo.save(prijatelj);
        System.out.println("\u001B[32m"+"\nSPREMAMO PRIJATELJA\n"+"\u001B[0m");
        System.out.println(prijatelj);
    }

    @Transactional
    public void deletePrijateljById(int id) {
        prijateljRepo.deleteByIdPrijateljstva(id);
    }

}
