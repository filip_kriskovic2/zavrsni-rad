package kriskovic.zavrsni.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kriskovic.zavrsni.model.ProgramVjezbanja;
import kriskovic.zavrsni.repository.ProgramVjezbanjaRepository;

@Service
public class ProgramVjezbanjaService {

    @Autowired
    private ProgramVjezbanjaRepository programRepo;

    public List<ProgramVjezbanja> getAllWorkoutPlans() {
        return programRepo.getAllWorkouts();
    }

    public ProgramVjezbanja getProgramById(int id)
    {
        Optional<ProgramVjezbanja> optional = programRepo.getByIdPrograma(id);
        if (optional.isPresent())
        {
            return optional.get();
        }
        else
        {
            throw new RuntimeException("Ne postoji program vjezbanja sa ovim idjem: " + id);
        }

    }
}
