package kriskovic.zavrsni.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Cilj {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idCilja;
    private String email;
    private Float ciljnaKilaza;
    private Float ciljnaKilometraza;
    private String nacin;
    private Float napredak;
    private String raspored;
    private boolean dovrseno = false;


    public Cilj() {
    }

    public int getIdCilja() {
        return this.idCilja;
    }

    public void setIdCilja(int idCilja) {
        this.idCilja = idCilja;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Float getCiljnaKilaza() {
        return this.ciljnaKilaza;
    }

    public void setCiljnaKilaza(Float ciljnaKilaza) {
        this.ciljnaKilaza = ciljnaKilaza;
    }

    public Float getCiljnaKilometraza() {
        return this.ciljnaKilometraza;
    }

    public void setCiljnaKilometraza(Float ciljnaKilometraza) {
        this.ciljnaKilometraza = ciljnaKilometraza;
    }

    public String getNacin() {
        return this.nacin;
    }

    public void setNacin(String nacin) {
        this.nacin = nacin;
    }

    public Float getNapredak() {
        return this.napredak;
    }

    public void setNapredak(Float napredak) {
        this.napredak = napredak;
    }

    public String getRaspored() {
        return this.raspored;
    }

    public void setRaspored(String raspored) {
        this.raspored = raspored;
    }

    public boolean getDovrseno() {
        return this.dovrseno;
    }

    public void setDovrseno(boolean dovrseno) {
        this.dovrseno = dovrseno;
    }

    @Override
    public String toString() {
        return "{" +
            " idCilja='" + getIdCilja() + "'" +
            ", email='" + getEmail() + "'" +
            ", ciljnaKilaza='" + getCiljnaKilaza() + "'" +
            ", ciljnaKilometraza='" + getCiljnaKilometraza() + "'" +
            ", nacin='" + getNacin() + "'" +
            ", napredak='" + getNapredak() + "'" +
            ", raspored='" + getRaspored() + "'" +
            "}";
    }


}
