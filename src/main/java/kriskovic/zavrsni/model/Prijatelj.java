package kriskovic.zavrsni.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Entity
public class Prijatelj {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPrijateljstva;
	private String prijateljEmail1;
	private String prijateljEmail2;

    public Prijatelj() {
    }

    public int getIdPrijateljstva() {
        return this.idPrijateljstva;
    }

    public void setIdPrijateljstva(int idPrijateljstva) {
        this.idPrijateljstva = idPrijateljstva;
    }

    public String getPrijateljEmail1() {
        return this.prijateljEmail1;
    }

    public void setPrijateljEmail1(String prijateljEmail1) {
        this.prijateljEmail1 = prijateljEmail1;
    }

    public String getPrijateljEmail2() {
        return this.prijateljEmail2;
    }

    public void setPrijateljEmail2(String prijateljEmail2) {
        this.prijateljEmail2 = prijateljEmail2;
    }

    @Override
    public String toString() {
        return "{" +
            " idPrijateljstva='" + getIdPrijateljstva() + "'" +
            ", prijateljEmail1='" + getPrijateljEmail1() + "'" +
            ", prijateljEmail2='" + getPrijateljEmail2() + "'" +
            "}";
    }


}