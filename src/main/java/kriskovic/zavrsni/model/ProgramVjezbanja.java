package kriskovic.zavrsni.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table( name = "programvjezbanja" )
public class ProgramVjezbanja {
    @Id
    private int idPrograma;
    private int intenzitet;
    private String naziv;
    private String opis;
    private String raspored;
    private int ukupno;  //ukupna kilometraza

    public ProgramVjezbanja() {
    }

    public int getIdPrograma() {
        return this.idPrograma;
    }

    public void setIdPrograma(int idPrograma) {
        this.idPrograma = idPrograma;
    }

    public int getIntenzitet() {
        return this.intenzitet;
    }

    public void setIntenzitet(int intenzitet) {
        this.intenzitet = intenzitet;
    }

    public String getNaziv() {
        return this.naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return this.opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getRaspored() {
        return this.raspored;
    }

    public void setRaspored(String raspored) {
        this.raspored = raspored;
    }

    public int getUkupno() {
        return this.ukupno;
    }

    public void setUkupno(int ukupno) {
        this.ukupno = ukupno;
    }

    @Override
    public String toString() {
        return "{" +
            " idPrograma='" + getIdPrograma() + "'" +
            ", intenzitet='" + getIntenzitet() + "'" +
            ", naziv='" + getNaziv() + "'" +
            ", opis='" + getOpis() + "'" +
            ", raspored='" + getRaspored() + "'" +
            ", ukupno='" + getUkupno() + "'" +
            "}";
    }

}
