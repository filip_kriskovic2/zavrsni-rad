package kriskovic.zavrsni.model;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
// import javax.persistence.JoinTable;
// import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.JoinColumn;


@Entity
public class Korisnik {
	@Id
	private String email;
	private String ime;
	private String prezime;
	private Long dob;
	private Float kilaza;
	private Float napredak;
	@JsonIgnore
	private byte[] lozinka;
	
	@ManyToOne
	@JoinColumn(name = "idprograma", referencedColumnName = "IdPrograma", insertable = false, updatable = true)    
	private ProgramVjezbanja programVjezbanja;

	// @JsonIgnore
	// @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    // @JoinTable(
    //     name = "Prijatelj",
    //     joinColumns = @JoinColumn(name = "prijateljemail1", referencedColumnName = "email"),  // email korisnika
    //     inverseJoinColumns = @JoinColumn(name = "prijateljemail2", referencedColumnName = "email"))  // email njegova prijatelja
	@OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "prijateljemail1", referencedColumnName = "email")
    private List<Prijatelj> prijatelji;  // ovo za sada radi na principu followera, a ne obostranih prijatelja

	@OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "email", referencedColumnName = "email")
    private List<Cilj> ciljevi;

	public Korisnik() {  // konstruktor
    }

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIme() {
		return this.ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return this.prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Long getDob() {
		return this.dob;
	}

	public void setDob(Long dob) {
		this.dob = dob;
	}

	public Float getKilaza() {
		return this.kilaza;
	}

	public void setKilaza(Float kilaza) {
		this.kilaza = kilaza;
	}

	public Float getNapredak() {
		return this.napredak;
	}

	public void setNapredak(Float napredak) {
		this.napredak = napredak;
	}

	public byte[] getLozinka() {
		return this.lozinka;
	}

	public void setLozinka(byte[] lozinka) {
		this.lozinka = lozinka;
	}

	public ProgramVjezbanja getProgramVjezbanja() {
		return this.programVjezbanja;
	}

	public void setProgramVjezbanja(ProgramVjezbanja programVjezbanja) {
		this.programVjezbanja = programVjezbanja;
	}

	public List<Prijatelj> getPrijatelji() {
		return this.prijatelji;
	}

	public void setPrijatelji(List<Prijatelj> prijatelji) {
		this.prijatelji = prijatelji;
	}

	public List<Cilj> getCiljevi() {
		return this.ciljevi;
	}

	public void setCiljevi(List<Cilj> ciljevi) {
		this.ciljevi = ciljevi;
	}


	@Override
	public String toString() {
		return "{" +
			"\nemail='" + getEmail() + "'" +
			",\nime='" + getIme() + "'" +
			",\nprezime='" + getPrezime() + "'" +
			",\ndob='" + getDob() + "'" +
			",\nkilaza='" + getKilaza() + "'" +
			",\nnapredak='" + getNapredak() + "'" +
			",\nlozinka='" + getLozinka() + "'" +
			",\nprogramVjezbanja='" + getProgramVjezbanja() + "'" +
			",\nprijatelji='" + getPrijatelji() + "'" +
			",\nciljevi='" + getCiljevi() + "'" +
			"\n}";
	}

	public boolean signinWrong()
	{
		if (this.getIme() == null || this.getPrezime() == null || this.getDob() == null
		|| this.getKilaza() == null ||this.getEmail() == null || this.getLozinka() == null) return true;
		return false;
	}

    public String validateUser() {
        if (this.getIme().length() < 2) {
    		return "First name needs to be at least 2 characters long.";
    	} else if (this.getPrezime().length() < 2) {
    		return "Last name needs to be at least 2 characters long.";
    	}
    	Pattern p = Pattern.compile("^([a-z0-9]+(?:[._-][a-z0-9]+)*)@([a-z0-9]+(?:[.-][a-z0-9]+)*\\.[a-z]{2,})$");
    	Matcher m = p.matcher(this.getEmail());
    	if (!(m.find()))
    		return "Email is in the wrong format.";
    	return "OK";
    }
	
}
