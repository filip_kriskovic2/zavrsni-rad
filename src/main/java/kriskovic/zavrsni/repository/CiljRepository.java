package kriskovic.zavrsni.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import kriskovic.zavrsni.model.Cilj;

@Repository
public interface CiljRepository extends JpaRepository<Cilj, Integer>{

    @Modifying
    void deleteByIdCilja(int idCilja);

    List<Cilj> findAllByNacin(String nacin);

    List<Cilj> findAll();

    List<Cilj> findAllByEmail(String email);
    
}
