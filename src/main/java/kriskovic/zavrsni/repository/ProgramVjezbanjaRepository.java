package kriskovic.zavrsni.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import kriskovic.zavrsni.model.ProgramVjezbanja;

@Repository
public interface ProgramVjezbanjaRepository extends JpaRepository<ProgramVjezbanja, Integer>{
    
    @Query("SELECT p FROM ProgramVjezbanja p")
    List<ProgramVjezbanja> getAllWorkouts();

    Optional<ProgramVjezbanja> getByIdPrograma(int idPrograma);

}
