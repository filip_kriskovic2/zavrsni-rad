package kriskovic.zavrsni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import kriskovic.zavrsni.model.Prijatelj;

@Repository
public interface PrijateljRepository extends JpaRepository<Prijatelj, Integer>{
    
    @Modifying
    void deleteByIdPrijateljstva(int id);
    
}