package kriskovic.zavrsni.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import kriskovic.zavrsni.model.Korisnik;

@Repository
public interface KorisnikRepository extends JpaRepository<Korisnik, Integer>{

    @Query("SELECT u FROM Korisnik u")
    List<Korisnik> getAllUsers();

    Optional<Korisnik> getByEmail(String email);

    @Query("SELECT u FROM Korisnik u JOIN FETCH u.ciljevi c WHERE u.email = ?1")
    Korisnik getKorisnikByEmailJoinCilj(String email);

}
