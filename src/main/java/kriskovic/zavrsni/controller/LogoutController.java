package kriskovic.zavrsni.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttribute;

import kriskovic.zavrsni.model.Korisnik;

@Controller
public class LogoutController {
    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return loggedUser;
    }


    @GetMapping("/odjaviSe")
    public String logout(HttpSession session, @SessionAttribute(required = false) Korisnik loggedUser)
    {
        session.invalidate();
        System.out.println("ODJAVA");
        return "redirect:/home";
    }
}
