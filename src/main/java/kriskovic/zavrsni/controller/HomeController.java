package kriskovic.zavrsni.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttribute;

import kriskovic.zavrsni.model.Korisnik;

@Controller
public class HomeController {
    
    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        System.out.printf("loggedUser=%s\n", loggedUser);
        return loggedUser;
    }

    @GetMapping({"/", "/home", ""})
    public String home()
    {
        return "home";
    }
}
