package kriskovic.zavrsni.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kriskovic.zavrsni.model.Korisnik;
import kriskovic.zavrsni.service.KorisnikService;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Random;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

@Controller
public class RegisterController {
    @Autowired
    private KorisnikService korisnikService;

    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return loggedUser;
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("user", new Korisnik());
        return "register";
    }

    @PostMapping("/registerUser")
    public String signUpUser(@ModelAttribute Korisnik korisnik, RedirectAttributes redAttrs) //throws IOException
    {
        byte[] secretkey;
        try
        {
            System.out.println("save korisnik");
            byte[] password = korisnik.getLozinka();
            char[] charpassword = new char[password.length];
            for (int i = 0; i < password.length; i++) {
                charpassword[i] = (char) password[i];
            }
            byte[] salt = new byte[16];
            new Random().nextBytes(salt);
            PBEKeySpec spec = new PBEKeySpec(charpassword, salt, 1000000, 128);
            SecretKeyFactory skf = null;
            try {
              skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
              secretkey = skf.generateSecret(spec).getEncoded();
            }
            catch (NoSuchAlgorithmException ignore) {
              return null;
            }
            catch (InvalidKeySpecException e) {
              throw new IllegalArgumentException(e);
            }
            byte[] c = new byte[salt.length + secretkey.length];
            System.arraycopy(salt, 0, c, 0, salt.length);
            System.arraycopy(secretkey, 0, c, salt.length, secretkey.length);
            korisnik.setLozinka(c);
            System.out.println(Arrays.toString(salt));
            System.out.println(Arrays.toString(secretkey));
            System.out.println(Arrays.toString(c));
            // korisnik.setLozinka(password);
            korisnikService.saveKorisnik(korisnik);
        }
        catch (RuntimeException err)
        {
            redAttrs.addFlashAttribute("error", err.getMessage());
            return "redirect:/register";
        }
        // catch (IOException err)
        // {
        //     redAttrs.addFlashAttribute("error", err.getMessage());
        //     return "redirect:/register";
        // }
        redAttrs.addFlashAttribute("success", "Registration successful!");
        return "redirect:/register";
    }
}
