package kriskovic.zavrsni.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kriskovic.zavrsni.model.Cilj;
import kriskovic.zavrsni.model.Korisnik;
import kriskovic.zavrsni.model.ProgramVjezbanja;
import kriskovic.zavrsni.service.CiljService;
import kriskovic.zavrsni.service.KorisnikService;
import kriskovic.zavrsni.service.ProgramVjezbanjaService;

@Controller  
public class WorkoutController {

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private CiljService ciljService;

    @Autowired
    private ProgramVjezbanjaService progService;

    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        System.out.printf("loggedUser=%s\n", loggedUser);
        return loggedUser;
    }

    @ModelAttribute("workoutlist")
    public List<Map<String,String>> workoutlist() {
        List<ProgramVjezbanja> workouts = progService.getAllWorkoutPlans();
        List<Map<String, String>> workoutlist = new ArrayList<>();
    
        workouts.stream().forEach(s -> {
            Map<String, String> mapa = new HashMap<>();
            mapa.put("naziv", s.getNaziv());
            mapa.put("id", Integer.toString(s.getIdPrograma()));
            mapa.put("intenzitet", Integer.toString(s.getIntenzitet()));
            mapa.put("ukupno", Integer.toString(s.getUkupno()));
            mapa.put("opis", s.getOpis());
            mapa.put("raspored", s.getRaspored());
            workoutlist.add(mapa);
        });
        return workoutlist;
    }

    @GetMapping("/workout")
    public String workout(Model model, HttpServletRequest request)
    {
        try{
            Korisnik loggedUser = (Korisnik) request.getSession().getAttribute("loggedUser");

            model.addAttribute("workoutplan", loggedUser.getProgramVjezbanja());
            model.addAttribute("goals", loggedUser.getCiljevi());
            // System.out.println(loggedUser.getCiljevi());
            return "workout";
        }
        catch (RuntimeException e) {
            return "redirect:/home";
        }
    }

    @PostMapping("/cancelWorkoutPlan")
    public String cancelWorkoutPlan(@RequestParam(name = "idPrograma") int idprograma, HttpServletRequest request, RedirectAttributes redAttrs){
        System.out.println("makni program " + idprograma);
        try {
            Korisnik loggedUser = (Korisnik) request.getSession().getAttribute("loggedUser");

            if (idprograma == loggedUser.getProgramVjezbanja().getIdPrograma())
            {
                loggedUser.setProgramVjezbanja(null);
                loggedUser.setNapredak(null);
                
                korisnikService.updateKorisnik(loggedUser);  //update baze
                redAttrs.addFlashAttribute("success", "Workout plan canceled");
    
                // return "redirect:/workout";
            } else {
                System.out.println("Wrong Workout ID");
                // return "redirect:/workout";
            }
        }
        catch (RuntimeException e) {
            redAttrs.addFlashAttribute("error", e.getMessage());
            System.out.println("runtime exception!!\n" + e);
            return "redirect:/workout";
        }
        return "redirect:/workout";
    }

    @PostMapping("/cancelGoal")
    public String cancelGoal(@ModelAttribute(value = "idCilja") int idcilja, HttpServletRequest request, RedirectAttributes redAttrs){
        System.out.println("makni cilj " + idcilja);
        try {
            Korisnik loggedUser = (Korisnik) request.getSession().getAttribute("loggedUser");
            for (int i = 0; i < loggedUser.getCiljevi().size(); i++) {
                if (loggedUser.getCiljevi().get(i).getIdCilja() == idcilja)
                {
                    Cilj cilj = loggedUser.getCiljevi().remove(i);
                    
                    ciljService.deleteCiljById(cilj.getIdCilja());  //update baze
                    redAttrs.addFlashAttribute("success", "Goals updated successfully");
        
                    return "redirect:/workout";
                }
            }
        }
        catch (RuntimeException e) {
            redAttrs.addFlashAttribute("error", e.getMessage());
            System.out.println("runtime exception!!\n" + e);
            return "redirect:/workout";
        }
        return "redirect:/workout";
    }

    @PostMapping("/addWorkout")
    public String addWorkout(@RequestParam(name = "idPrograma") String idprograma, HttpServletRequest request, RedirectAttributes redAttrs)
    {
        System.out.println("dodaj program " + idprograma);
        try {
            Korisnik loggedUser = (Korisnik) request.getSession().getAttribute("loggedUser");
            ProgramVjezbanja program = progService.getProgramById(Integer.parseInt(idprograma));

            loggedUser.setProgramVjezbanja(program);
            loggedUser.setNapredak(0.0f);
            
            korisnikService.updateKorisnik(loggedUser);  //update baze
            redAttrs.addFlashAttribute("success", "Workout plan added");

            return "redirect:/workout";
        }
        catch (RuntimeException e) {
            redAttrs.addFlashAttribute("error", e.getMessage());
            System.out.println("runtime exception!!\n" + e);
            return "redirect:/workout";
        }
    }

    @GetMapping("/recordWorkout")
    public String recordWorkout()
    {
        return "recordworkout";
    }

    @PostMapping("/addRecord")
    public String addRecord(@RequestParam(name = "kilometraza") Float kilometraza, @RequestParam(name = "nacin") String nacin, RedirectAttributes redAttrs,  HttpServletRequest request)
    {
        System.out.println("zabiljezi workout " + kilometraza + "km " + nacin);
        try {
            Korisnik loggedUser = (Korisnik) request.getSession().getAttribute("loggedUser");
            
            if (loggedUser.getProgramVjezbanja().getNaziv().contains(nacin)) {
                Float novinapredak = loggedUser.getNapredak() + kilometraza;
                loggedUser.setNapredak(novinapredak);
                korisnikService.updateKorisnik(loggedUser);
            }

            List<Cilj> ciljevi = ciljService.getAllByEmail(loggedUser.getEmail());
            for (int i = 0; i < ciljevi.size(); i++) {
                System.out.println(ciljevi.get(i).getNacin().equals(nacin));
                if (ciljevi.get(i).getNacin().equals(nacin)) {
                    Float napredak = ciljevi.get(i).getNapredak();
                    Float ciljnakilometraza = ciljevi.get(i).getCiljnaKilometraza();
    
                    Float novinapredak2 = napredak + kilometraza;
                    ciljevi.get(i).setNapredak(novinapredak2);
                    if (ciljevi.get(i).getCiljnaKilaza() != null) {
                        if (novinapredak2 >= ciljnakilometraza && loggedUser.getKilaza() <= ciljevi.get(i).getCiljnaKilaza()) {
                            ciljevi.get(i).setDovrseno(true);
                        }
                    } else {
                        if (novinapredak2 >= ciljnakilometraza) {
                            ciljevi.get(i).setDovrseno(true);
                        }
                    }
                    ciljService.saveCilj(ciljevi.get(i));
                }
            }

            loggedUser.setCiljevi(ciljevi);

            redAttrs.addFlashAttribute("success", "Workout recorded");
            return "redirect:/workout";
        }
        catch (RuntimeException e) {
            redAttrs.addFlashAttribute("error", e.getMessage());
            System.out.println("runtime exception!!\n" + e);
            return "redirect:/recordWorkout";
        }
    }

}
