package kriskovic.zavrsni.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kriskovic.zavrsni.model.Cilj;
import kriskovic.zavrsni.model.Korisnik;
import kriskovic.zavrsni.service.CiljService;

@Controller
public class GoalController {
    
    @Autowired
    private CiljService ciljService;

    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        System.out.printf("loggedUser=%s\n", loggedUser);
        return loggedUser;
    }

    @GetMapping("setGoal")
    public String setGoal(Model model)
    {
        model.addAttribute("cilj", new Cilj());
        return "setGoal";
    }

    @PostMapping("/addGoal")
    public String addGoal(@ModelAttribute Cilj cilj, RedirectAttributes redAttrs,  HttpServletRequest request)
    {;
        if (cilj.getCiljnaKilaza() == null && cilj.getCiljnaKilometraza() == null) {
            redAttrs.addFlashAttribute("error", "Please set either 'Target Weight' or 'Target Mileage'");
            return "redirect:/setGoal";
        } else if (cilj.getNacin() == null) {
            redAttrs.addFlashAttribute("error", "Please set 'Way of exercising'");
            return "redirect:/setGoal";
        } else if (cilj.getNacin().equals("Cycling") && cilj.getNacin().equals("Running") && cilj.getNacin().equals("Easy walking")) {
            redAttrs.addFlashAttribute("error", "Please select an existing way of exercising");
            return "redirect:/setGoal";
        } else if ((cilj.getCiljnaKilaza() != null && cilj.getCiljnaKilaza() <= 0) || (cilj.getCiljnaKilometraza() != null && cilj.getCiljnaKilometraza() <= 0)) {
            redAttrs.addFlashAttribute("error", "Please enter a number greater than 0");
            return "redirect:/setGoal";
        }

        try
        {
            Korisnik loggedUser = (Korisnik) request.getSession().getAttribute("loggedUser");

            if (cilj.getCiljnaKilaza() != null && cilj.getCiljnaKilaza() >= loggedUser.getKilaza()) {
                redAttrs.addFlashAttribute("error", "Please set 'Target Weight' that is less than your current weight");
                return "redirect:/setGoal";
            }

            System.out.println("add goal");
            cilj.setEmail(loggedUser.getEmail());
            cilj.setDovrseno(false);
            cilj.setNapredak(0.0f);
            if (cilj.getCiljnaKilometraza() == null) {
                Float kilaza = cilj.getCiljnaKilaza();
                if (cilj.getNacin().equals("Cycling")) {
                    cilj.setCiljnaKilometraza(Math.round((loggedUser.getKilaza() - kilaza) * 460 * 100)/100f);
                } else if (cilj.getNacin().equals("Running")) {
                    cilj.setCiljnaKilometraza(Math.round((loggedUser.getKilaza() - kilaza) * 196 * 100)/100f);
                } else if (cilj.getNacin().equals("Easy walking")) {
                    cilj.setCiljnaKilometraza(Math.round((loggedUser.getKilaza() - kilaza) * 190 * 100)/100f);
                }
            }
            System.out.println(cilj);
            List<Cilj> ciljevi = loggedUser.getCiljevi();
            ciljevi.add(cilj);
            loggedUser.setCiljevi(ciljevi);
            ciljService.saveCilj(cilj);
        }
        catch (RuntimeException err)
        {
            redAttrs.addFlashAttribute("error", err.getMessage());
            return "redirect:/setGoal";
        }
        redAttrs.addFlashAttribute("success", "Goal added");
        return "redirect:/workout";
    }
}
