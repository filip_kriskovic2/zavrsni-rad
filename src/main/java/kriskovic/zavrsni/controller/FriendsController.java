package kriskovic.zavrsni.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kriskovic.zavrsni.model.Korisnik;
import kriskovic.zavrsni.model.Prijatelj;
import kriskovic.zavrsni.model.ProgramVjezbanja;
import kriskovic.zavrsni.service.KorisnikService;
import kriskovic.zavrsni.service.PrijateljService;

@Controller
public class FriendsController {
    
    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private PrijateljService prijateljService;

    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        System.out.printf("loggedUser=%s\n", loggedUser);
        return loggedUser;
    }

    @ModelAttribute("friends")
    public List<Map<String,String>> workoutlist(HttpServletRequest request) {
        Korisnik loggedUser = (Korisnik) request.getSession().getAttribute("loggedUser");
        List<Prijatelj> friends = loggedUser.getPrijatelji();
        List<Map<String, String>> friendlist = new ArrayList<>();
    
        friends.stream().forEach(s -> {
            Map<String, String> mapa = new HashMap<>();
            mapa.put("ime", korisnikService.getKorisnikByEmail(s.getPrijateljEmail2()).getIme());
            mapa.put("prezime", korisnikService.getKorisnikByEmail(s.getPrijateljEmail2()).getPrezime());
            mapa.put("email", korisnikService.getKorisnikByEmail(s.getPrijateljEmail2()).getEmail());
            mapa.put("idprijateljstva", Integer.toString(s.getIdPrijateljstva()));
            friendlist.add(mapa);
        });
        return friendlist;
    }

    @ModelAttribute("friendsworkoutlist")
    public List<Map<String,String>> friendsworkoutlist(HttpServletRequest request) {
        Korisnik loggedUser = (Korisnik) request.getSession().getAttribute("loggedUser");
        List<Prijatelj> friends = loggedUser.getPrijatelji();

        List<Map<String, String>> friendsworkoutlist = new ArrayList<>();
        
        friends.stream().forEach(f -> {
            try {
                ProgramVjezbanja s = korisnikService.getKorisnikByEmail(f.getPrijateljEmail2()).getProgramVjezbanja();
                Map<String, String> mapa = new HashMap<>();
                mapa.put("naziv", s.getNaziv());
                mapa.put("id", Integer.toString(s.getIdPrograma()));
                mapa.put("intenzitet", Integer.toString(s.getIntenzitet()));
                mapa.put("ukupno", Integer.toString(s.getUkupno()));
                mapa.put("opis", s.getOpis());
                mapa.put("raspored", s.getRaspored());
                mapa.put("friendemail", f.getPrijateljEmail2());
                mapa.put("friendnapredak", Float.toString(korisnikService.getKorisnikByEmail(f.getPrijateljEmail2()).getNapredak()));
                mapa.put("friendnapredakpostotak", Float.toString(korisnikService.getKorisnikByEmail(f.getPrijateljEmail2()).getNapredak() / s.getUkupno() * 100));
                friendsworkoutlist.add(mapa);
            } catch (NullPointerException e) {
                System.out.println("Null Pointer Exception " + e);
                System.out.println("Prijatelj nema program vjezbanja: " + f.getPrijateljEmail2());
            }
        });
        return friendsworkoutlist;
    }

    @GetMapping("/friends")
    public String friends()
    {
        return "friends";
    }

    @PostMapping("/addFriend")
    public String addFriend(@RequestParam(name = "friendEmail") String friendemail, RedirectAttributes redAttrs,  HttpServletRequest request)
    {
        System.out.println("dodaj prijatelja " + friendemail);
        try {
            Korisnik loggedUser = (Korisnik) request.getSession().getAttribute("loggedUser");
            // Korisnik prijatelj = korisnikService.getKorisnikByEmailJoinCilj(friendemail);

            // prijatelj.setPrijatelji(null);

            // List<Korisnik> prijatelji = loggedUser.getPrijatelji();
            // prijatelji.add(prijatelj);

            //novo
            if (korisnikService.getKorisnikByEmail(friendemail) == null) {
                throw new RuntimeException("There is no user with this email: " + friendemail);
            } else if (korisnikService.getKorisnikByEmail(friendemail).getEmail().equals(loggedUser.getEmail())) {
                throw new RuntimeException("You can't follow yourself");
            }

            for (int i = 0; i < loggedUser.getPrijatelji().size(); i++) {
                if (loggedUser.getPrijatelji().get(i).getPrijateljEmail2().equals(friendemail)) {
                    throw new RuntimeException("You already follow this person");
                }
            }

            Prijatelj prijatelj = new Prijatelj();
            prijatelj.setPrijateljEmail1(loggedUser.getEmail());
            prijatelj.setPrijateljEmail2(friendemail);
            List<Prijatelj> prijatelji = loggedUser.getPrijatelji();
            prijatelji.add(prijatelj);
            loggedUser.setPrijatelji(prijatelji);
            prijateljService.savePrijatelj(prijatelj);
            // korisnikService.updateKorisnik(loggedUser);  //update baze
            redAttrs.addFlashAttribute("success", "Friend added");

            return "redirect:/friends";
        }
        catch (RuntimeException e) {
            redAttrs.addFlashAttribute("error", e.getMessage());
            System.out.println("runtime exception!!\n" + e);
            return "redirect:/friends";
        }
    }

    @PostMapping("/removeFriend")
    public String cancelGoal(@ModelAttribute(value = "idprijateljstva") int idprijateljstva, HttpServletRequest request, RedirectAttributes redAttrs){
        System.out.println("makni prijateljstvo " + idprijateljstva);
        try {
            Korisnik loggedUser = (Korisnik) request.getSession().getAttribute("loggedUser");
            for (int i = 0; i < loggedUser.getPrijatelji().size(); i++) {
                if (loggedUser.getPrijatelji().get(i).getIdPrijateljstva() == idprijateljstva)
                {
                    Prijatelj prijatelj = loggedUser.getPrijatelji().remove(i);
                    
                    prijateljService.deletePrijateljById(prijatelj.getIdPrijateljstva());  //update baze
                    redAttrs.addFlashAttribute("success", "Friends updated successfully");
        
                    return "redirect:/friends";
                }
            }
        }
        catch (RuntimeException e) {
            redAttrs.addFlashAttribute("error", e.getMessage());
            System.out.println("runtime exception!!\n" + e);
            return "redirect:/friends";
        }
        return "redirect:/friends";
    }

    @GetMapping("/friendsWorkout")
    public String friendsWorkout()
    {
        return "friendsWorkout";
    }
}