package kriskovic.zavrsni.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kriskovic.zavrsni.model.Cilj;
import kriskovic.zavrsni.model.Korisnik;
import kriskovic.zavrsni.service.CiljService;
import kriskovic.zavrsni.service.KorisnikService;

@Controller
public class ProfileController {
    
    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private CiljService ciljService;

    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        System.out.printf("loggedUser=%s\n", loggedUser);
        return loggedUser;
    }

    @GetMapping("/profile")
    public String profile(Model model)
    {
        model.addAttribute("user", new Korisnik());
        return "profile";
    }

    @PostMapping("/editprofile")
    public String editProfile(@ModelAttribute(value = "user") Korisnik korisnik, HttpServletRequest request, RedirectAttributes redAttrs)
    {
        System.out.println("PROMJENA PROFILA: " + korisnik);
        try {
            Korisnik loggedUser = (Korisnik) request.getSession().getAttribute("loggedUser");

            if (loggedUser.getIme().equals(korisnik.getIme()) &&  //provjera je li doslo do promjena kako se ne bi radio beskoristan upit u bazu
                loggedUser.getPrezime().equals(korisnik.getPrezime()) &&
                loggedUser.getDob().equals(korisnik.getDob()) &&
                loggedUser.getKilaza().equals(korisnik.getKilaza()))
            {
                System.out.println("no changes made to profile");
                return "redirect:/profile";
            } else {
                loggedUser.setIme(korisnik.getIme());
                loggedUser.setPrezime(korisnik.getPrezime());
                loggedUser.setDob(korisnik.getDob());
                loggedUser.setKilaza(korisnik.getKilaza());
                
                List<Cilj> ciljevi = ciljService.getAllByEmail(loggedUser.getEmail());
                for (int i = 0; i < ciljevi.size(); i++) {
                    if (ciljevi.get(i).getCiljnaKilaza() != null) {
                        if (ciljevi.get(i).getNapredak() >= ciljevi.get(i).getCiljnaKilometraza() && loggedUser.getKilaza() <= ciljevi.get(i).getCiljnaKilaza()) {
                            ciljevi.get(i).setDovrseno(true);
                            loggedUser.setCiljevi(ciljevi);
                            ciljService.saveCilj(ciljevi.get(i));
                        }
                    }
                }

                korisnikService.updateKorisnik(loggedUser);  //update baze
                redAttrs.addFlashAttribute("success", "Profile updated successfully");
    
                return "redirect:/profile";
            }
        }
        catch (RuntimeException e) {
            redAttrs.addFlashAttribute("error", e.getMessage());
            return "redirect:/profile";
        }
    }
}
