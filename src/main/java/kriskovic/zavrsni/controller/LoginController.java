package kriskovic.zavrsni.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kriskovic.zavrsni.model.Korisnik;
import kriskovic.zavrsni.service.KorisnikService;

@Controller
public class LoginController {
    
    @Autowired
    private KorisnikService korisnikService;
    
    @GetMapping("/login")
    public String login(Model model)
    {
        model.addAttribute("user", new Korisnik());
        return "login";
    }

    @PostMapping("/login")
    public String loginUser(String email, String lozinka, HttpServletRequest request, RedirectAttributes redAttrs)
    {
        System.out.println(email + " " + lozinka);
        try {
            Korisnik korisnik = korisnikService.logginKorisnik(email, lozinka);
            request.getSession().setAttribute("loggedUser", korisnik);
            return "redirect:/";
        }
        catch (RuntimeException e) {
            redAttrs.addFlashAttribute("error", e.getMessage());
            return "redirect:/login";
        }
    }
}
